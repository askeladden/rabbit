import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;

import net.jodah.lyra.ConnectionOptions;
import net.jodah.lyra.Connections;
import net.jodah.lyra.config.Config;
import net.jodah.lyra.config.ConfigurableConnection;
import net.jodah.lyra.config.RecoveryPolicies;
import net.jodah.lyra.config.RetryPolicy;
import net.jodah.lyra.util.Duration;

public class KuracPublisher {
	public static final String QUEUE_NAME = "kurac.secretmessage";
	public static final String EXCHANGE_NAME = "kurac.exchange";
	public static final String EXCHNAGE_TYPE = "topic";
	public static final String ROUTING_KEY = "kurac.secrets";

	
	public static void main (String[] args) throws IOException, TimeoutException{
		KuracPublisher test = new KuracPublisher();
		Channel channel = KuracPublisher.createConnection();
		test.run(channel);
}


	private void run(Channel channel) {
	while(true){
		try {
			Thread.sleep(10);
		channel.exchangeDeclare(EXCHANGE_NAME, EXCHNAGE_TYPE, true);
		 channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		    String message = "Hello kurac";
		    channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, null, message.getBytes());
		    System.out.println(" [x] Sent '" + message + "'");
		} catch (Exception e) {
			System.out.println("Disturbed while sleeping");
		}
	}
	
}


	public static Channel createConnection() throws IOException, TimeoutException{
		Address[] addr = new Address[3];
		addr[0] = new Address("10.2.0.11");
		addr[1] = new Address("10.2.0.22");
		addr[2] = new Address("10.2.0.33");
		
		Config config = new Config()
			    .withRecoveryPolicy(RecoveryPolicies.recoverAlways())
			    .withRetryPolicy(new RetryPolicy()
			        .withMaxAttempts(1000000)
			        .withInterval(Duration.seconds(1))
			        .withMaxDuration(Duration.minutes(5)));
		
	    ConnectionOptions options = new ConnectionOptions()
	    		.withAddresses(addr)
	    		.withUsername("javakurac")
	    		.withPassword("javapacan")
	    		.withVirtualHost("kuracvhost");
	    ConfigurableConnection connection = Connections.create(options, config);

	    Channel channel = connection.createChannel();
	    return channel;
	}
}
