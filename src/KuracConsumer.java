import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class KuracConsumer {

	
	public static void main(String[] args) throws IOException, TimeoutException {
		KuracConsumer consumer = new KuracConsumer();
		Channel channel = KuracPublisher.createConnection();
		consumer.run(channel);
	}

	private void run(Channel channel) throws IOException {
				channel.exchangeDeclare(KuracPublisher.EXCHANGE_NAME, KuracPublisher.EXCHNAGE_TYPE, true);
				channel.queueDeclare(KuracPublisher.QUEUE_NAME, true, false, false, null);
				channel.queueBind(KuracPublisher.QUEUE_NAME, KuracPublisher.EXCHANGE_NAME, KuracPublisher.ROUTING_KEY);
				
				Consumer consumer = new DefaultConsumer(channel) {
				      @Override
				      public void handleDelivery(String consumerTag, Envelope envelope,
				                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
				        String message = new String(body, "UTF-8");
				        System.out.println(" [x] Received '" + message + "'");
				      }
				    };
				    channel.basicConsume(KuracPublisher.QUEUE_NAME, true, consumer);
	}
}
